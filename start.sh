#!/usr/bin/env bash

echo "Starting with UID=${PUID} (${U}), GID=${PGID} (${G}) in ${WD}"

userdel -f $U 2>/dev/null
groupdel -f $G 2>/dev/null

groupadd -g $PGID $G
useradd -d "${WD}" -u $PUID -g $PGID $U

chown -R $U\:$G $WD/config
chown -R $U\:$G $WD/data

mkdir dbox
ln -s $WD/config $WD/dbox/.dropbox
ln -s $WD/data   $WD/dbox/Dropbox
chown -R $U\:$G .

sudo -i -u $U bash -c 'cd '$WD' && wget -q -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -'
sudo -i -u $U bash -c 'cd '$WD' && HOME='$WD'/dbox ./.dropbox-dist/dropboxd'
while [[ 1 ]]; do
  echo "Dropbox stopped itself"
  sleep 86400 
done

