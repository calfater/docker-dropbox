FROM calfater/base-image:ubuntu-18.04

ARG U=dropbox
ARG G=$U

ENV U=$U \
    G=$G \
    WD=/$U \
    PUID=1000 \
    PGID=1000

WORKDIR $WD

RUN apt-get update && \
    apt-get install -yq libatomic1 libglapi-mesa libxdamage1 libxfixes3 libxcb-glx0 libxcb-dri2-0 libxcb-dri3-0 libxcb-present0 libxcb-sync1 libxshmfence1 libxxf86vm1 && \
    rm -rf /var/lib/apt/lists/*
    
RUN mkdir /config && \
    mkdir /data 

COPY start.sh start.sh 

RUN chmod +x start.sh

VOLUME ["$WD/config", "$WD/data"]

ENTRYPOINT ["./start.sh"]
